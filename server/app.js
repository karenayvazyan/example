var express = require("express");
var app = express();
var path=require('path');
app.get("/", function(request, response){
    response.sendFile('index.html',{root: path.join(__dirname, './public/')})
});
app.listen(3001,()=>{
    console.log("Server running in port 3001 ");
});